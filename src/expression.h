#pragma once

#include <numeric>
#include <string>
#include <vector>
#include <string_util/str_join.h>
#include "sql_formatter.h"


class Expression
{
public:

    Expression(const std::string& atom)
    {
        push(atom);
    }


    Expression& is_null()
    {
        return push(" IS NULL");
    }


    Expression& is_not_null()
    {
        return push(" IS NOT NULL");
    }


    template<typename Value>
    Expression& operator==(Value&& val)
    {
        return push(" =", sql_formatter()(val));
    }


    template<typename Value>
    Expression& operator!=(Value&& val)
    {
        return push(" !=", sql_formatter()(val));
    }


    template<typename Value>
    Expression& operator<=(Value&& val)
    {
        return push(" <=", sql_formatter()(val));
    }


    template<typename Value>
    Expression& operator<(Value&& val)
    {
        return push(" <", sql_formatter()(val));
    }


    template<typename Value>
    Expression& operator>=(Value&& val)
    {
        return push(" >=", sql_formatter()(val));
    }


    template<typename Value>
    Expression& operator>(Value&& val)
    {
        return push(" >", sql_formatter()(val));
    }


    template<typename Value>
    Expression& operator%(Value&& val)
    {
        return push(" %", sql_formatter()(val));
    }


    /*
     * Condition is ommitted if val is empty
     */
    template<typename V>
    Expression& operator<=>(V&& val)
    {
        /*
         * If null string (\0)...
         */
        bool empty = false;
        if constexpr (
                std::is_same_v<
                    std::remove_cv_t<
                        std::remove_reference_t<V>>, char[1]>
                        )
        {
            empty = true;
        }
        /*
         * ...else if empty string...
         */
        if constexpr (
                std::is_same_v<
                    std::remove_cv_t<
                        std::remove_reference_t<V>>, std::string>
                        )
        {
            if (val.empty())
            {
                empty = true;
            }
        }
        if (empty)
        {
            pop(); // column name
            pop(); // keyword (e.g. AND)
            return *this;
        }
        /*
         * ...else construct where clause
         */
        return *this==(val);
    }


    template<typename T>
    Expression& operator&&(T&& arg)
    {
        const std::string& s = arg.str();
        if (s.empty())
        {
            return *this;
        }
        if (!_data.empty())
        {
            push(" AND\n ");
        }
        return push(s);
    }


    template<typename T>
    Expression& operator||(T&& arg)
    {
        const std::string& s = arg.str();
        if (s.empty())
        {
            return *this;
        }
        if (!_data.empty())
        {
            push(" OR\n ");
        }
        return push(s);
    }


    template<typename ...Values>
    Expression& in(Values&&... vals)
    {
        return push(" IN", "(", join<',', sql_formatter>(vals...), ")");
    }


    template<typename V1, typename V2>
    Expression& between(V1&& v1, V2&& v2)
    {
        return push(" BETWEEN", sql_formatter()(v1), "AND", sql_formatter()(v2));
    }


    std::string str() const
    {
        return std::accumulate(_data.cbegin(), _data.cend(), std::string{});
    }


private:

    template<typename ...Values>
    Expression& push(Values&&... vals)
    {
        _data.push_back(join<' '>(vals...));
        return *this;
    }


    void pop()
    {
        if (_data.empty())
        {
            return;
        }
        _data.pop_back();
    }


protected:

    std::vector<std::string> _data;
};


template<>
inline
Expression& Expression::operator==(Expression&& val)
{
    return push(" =", val.str());
}


template<>
inline
Expression& Expression::operator<=(Expression&& val)
{
    return push(" <=", val.str());
}


template<>
inline
Expression& Expression::operator<(Expression&& val)
{
    return push(" <", val.str());
}


template<>
inline
Expression& Expression::operator>=(Expression&& val)
{
    return push(" >=", val.str());
}


template<>
inline
Expression& Expression::operator>(Expression&& val)
{
    return push(" >", val.str());
}
