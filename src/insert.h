#pragma once

#include <string>
#include <string_util/str_join.h>
#include "sql_formatter.h"
#include "statement.h"


class InsertStatement : public Statement<InsertStatement>
{
public:

    InsertStatement(const std::string& table) :
        _table(table)
    { }


    template<typename ...Columns>
    InsertStatement& columns(Columns&&... cols)
    {
        return columns_string("(" + join<','>(cols...) + ")");
    }


    template<typename ...Columns>
    InsertStatement& columns_string(const std::string& cols)
    {
        _cols = cols;
        return *this;
    }


    template<typename ...Values>
    InsertStatement& values(Values&&... vals)
    {
        return values_string("\n(" + join<',', sql_formatter>(vals...) + "),");
    }


    template<typename ...Values>
    InsertStatement& values_string(const std::string& vals)
    {
        _vals += vals;
        return *this;
    }


    template<typename ...Values>
    InsertStatement& operator()(Values&&... vals)
    {
        return values(vals...);
    }


    InsertStatement& on_conflict(const std::string conflict)
    {
        _conflict = conflict;
        return *this;
    }


    InsertStatement& on_conflict_do_nothing()
    {
        return on_conflict("ON CONFLICT DO NOTHING");
    }


    template<typename ...Returning>
    InsertStatement& returning(Returning&&... ret)
    {
        _ret = join<','>(ret...);
        return *this;
    }


    template<typename ...Returning>
    InsertStatement& returning_id()
    {
        return returning("RETURNING id");
    }


    void clear_values()
    {
        _vals.clear();
    }


    std::string query_string() const
    {
        if (_vals.empty())
        {
            return "SELECT;";
        }
        return join<'\n'>(
                "INSERT INTO"                     ,
                _table                            ,
                _cols                             ,
                "VALUES"                          ,
                _vals.substr(0, _vals.size() - 1) ,
                _conflict                         ,
                _ret                              ,
                ";"
                );
    }


private:

    const std::string _table;

    std::string _cols;

    std::string _vals;

    std::string _conflict;

    std::string _ret;
};


inline
InsertStatement insert_into(const std::string& table)
{
    return InsertStatement(table);
}
