#pragma once

#include <string>
#include "execute.h"


template<typename Derived>
class Statement
{
public:

    ExecuteStatement connect(
            const std::string& dbname               ,
            const std::string& user   = "postgres"  ,
            const std::string& pw     = "postgres"  ,
            const std::string& host   = "localhost" ,
            const std::string& port   = "5432"
            )
    {
        return ExecuteStatement(
                static_cast<Derived&>(*this).query_string() ,
                dbname                                      ,
                user                                        ,
                pw                                          ,
                host                                        ,
                port
                );
    }
};
