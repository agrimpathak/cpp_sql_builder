#pragma once

#include <cmath>
#include <string>
#include <string_util/str_join.h>
#include "util.h"


struct sql_formatter
{
    template<typename T>
    std::string operator()(T&& arg) const
    {
        /*
         * Single char
         */
        if constexpr (
                std::is_same_v<
                    std::remove_cv_t<
                        std::remove_reference_t<T>>, char>
                        )
        {
            if (arg == '\0')
            {
                return "NULL";
            }
            return quote(std::string(1, arg));
        }
        /*
         * Null string (\0)
         */
        if constexpr (
                std::is_same_v<
                    std::remove_cv_t<
                        std::remove_reference_t<T>>, char[1]>
                        )
        {
            return "NULL";
        }
        /*
         * String
         */
        if constexpr (
                std::is_same_v<
                    std::remove_cv_t<
                        std::remove_reference_t<T>>, std::string>
                        )
        {
            return (arg.empty() || arg == "NULL") ? "NULL" : quote(arg);
        }
        /*
         * Boolean
         */
        if constexpr (
                std::is_same_v<
                    std::remove_cv_t<
                        std::remove_reference_t<T>>, bool>
                        )
        {
            return arg ? "true" : "false";
        }
        /*
         * Number
         */
        if constexpr (
                std::is_arithmetic_v<
                    std::remove_cv_t<
                        std::remove_reference_t<T>>>
                        )
        {
            if (std::isnan(arg))
            {
                return "NULL";
            }
            return std::to_string(arg);
        }
        /*
         * ...Everything else (e.g. char[N], objects etc)
         */
        return quote(std::to_string(arg));
    }
};
