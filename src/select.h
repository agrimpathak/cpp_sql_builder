#pragma once

#include <string>
#include <string_util/str_join.h>
#include "expression.h"
#include "join.h"
#include "statement.h"


class SelectStatement : public Statement<SelectStatement>
{

friend class Join<SelectStatement>;

public:

    SelectStatement(const std::string& select, const std::string& fields) :
        _select(select),
        _fields(fields)
    { }


    SelectStatement& from(const std::string& from)
    {
        _from = from;
        return *this;
    }


    template<typename ...Args>
    SelectStatement& from_function(const std::string& func, Args&&... args)
    {
        _from = func + "(" + join<','>(args...) + ")";
        return *this;
    }


    InnerJoin<SelectStatement> inner_join(const std::string& table)
    {
        return InnerJoin(*this, table);
    }


    LeftJoin<SelectStatement> left_join(const std::string& table)
    {
        return LeftJoin(*this, table);
    }


    RightJoin<SelectStatement> right_join(const std::string& table)
    {
        return RightJoin(*this, table);
    }


    SelectStatement& where(const Expression& expr)
    {
        const std::string prefix = _where.empty() ? "WHERE\n" : "\nAND\n";
        _where += prefix + "(" + expr.str() + ")";
        return *this;
    }


    template<typename ...S>
    SelectStatement& group_by(S&&... fields)
    {
        _group_by = "GROUP BY\n" + join<','>(fields...);
        return *this;
    }


    template<typename ...S>
    SelectStatement& order_by(S&&... fields)
    {
        _order_by = "ORDER BY\n" + join<','>(fields...);
        return *this;
    }


    std::string query_string() const
    {
        return join<'\n'>(
                _select     ,
                _fields     ,
                "FROM"      ,
                _from       ,
                _join       ,
                _where      ,
                _group_by   ,
                _order_by   ,
                ";"
                );
    }


private:

    SelectStatement& append_join(const std::string& join)
    {
        _join += join + '\n';
        return *this;
    }


private:

    const std::string _select;

    const std::string _fields;

    std::string _from;

    std::string _join;

    std::string _where;

    std::string _group_by;

    std::string _order_by;
};


template<typename ...S>
SelectStatement _select_(const std::string& select, S&&... fields)
{
    return SelectStatement(select, join<','>(fields...));
}


template<typename ...S>
SelectStatement select(S&&... fields)
{
    return _select_("SELECT", fields...);
}


template<typename ...S>
SelectStatement select_distinct(S&&... fields)
{
    return _select_("SELECT DISTINCT", fields...);
}
