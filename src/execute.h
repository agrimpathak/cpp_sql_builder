#pragma once

#include <pqxx/pqxx>
#include <string>
#include "dao.h"


class ExecuteStatement
{
public:

    ExecuteStatement(
        const std::string& qs                     ,
        const std::string& dbname                 ,
        const std::string& user     = "postgres"  ,
        const std::string& password = "postgres"  ,
        const std::string& host     = "localhost" ,
        const std::string& port     = "5432"
        ) :
        _qs(qs)                                  ,
        _dao(dbname, user, password, host, port)
    { }


    pqxx::result execute()
    {
        return _dao.execute(_qs);
    }


private:

    const std::string _qs;

    Dao _dao;
};
