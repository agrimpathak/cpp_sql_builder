#pragma once

#include <string>
#include <string_util/str_join.h>
#include "expression.h"


template<typename Callee>
class Join
{
public:

    Join(Callee& callee, const std::string& table, const std::string& join_type) :
        _callee(callee)       ,
        _table(table)         ,
        _join_type(join_type)
    { }


    Callee& on(const Expression& expr)
    {
        _cond = expr.str();
        return _callee.append_join(str());
    }


    std::string str() const
    {
        return join<' '>(_join_type, _table, "ON", _cond);
    }


private:

    Callee& _callee;

    const std::string _table;

    const std::string _join_type;

    std::string _cond;
};


template<typename Callee>
class InnerJoin : public Join<Callee>
{
public:

    InnerJoin(Callee& callee, const std::string& table) :
        Join<Callee>(callee, table, "INNER JOIN")
    { }
};


template<typename Callee>
class LeftJoin : public Join<Callee>
{
public:

    LeftJoin(Callee& callee, const std::string& table) :
        Join<Callee>(callee, table, "LEFT JOIN")
    { }
};


template<typename Callee>
class RightJoin : public Join<Callee>
{
public:

    RightJoin(Callee& callee, const std::string& table) :
        Join<Callee>(callee, table, "RIGHT JOIN")
    { }
};
