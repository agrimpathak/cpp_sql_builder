#pragma once

#include <string>
#include "util.h"


template<typename T, typename ...B>
std::string nullif_and(T&& arg, B&&... b)
{
    return variadic_and(b...) ? "NULL" : std::to_string(arg);
}


template<typename T, typename ...B>
std::string nullif_or(T&& arg, B&&... b)
{
    return variadic_or(b...) ? "NULL" : std::to_string(arg);
}


template<typename T, typename V, typename ...B>
std::string nullif(T&& arg, V&& val)
{
    return nullif_and(arg, arg == val);
}
