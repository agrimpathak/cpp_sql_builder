#pragma once

#include <string>
#include <pqxx/pqxx>
#include <string_util/str_join.h>


inline
std::string connection_string(
        const std::string& dbname               ,
        const std::string& user   = "postgres"  ,
        const std::string& pw     = "postgres"  ,
        const std::string& host   = "localhost" ,
        const std::string& port   = "5432"
        )
{
    return join<' '>(
            "dbname   = " , dbname ,
            "user     = " , user   ,
            "password = " , pw     ,
            "host     = " , host   ,
            "port     = " , port
            );
}


class Dao
{
public:

    Dao(const std::string& dbname               ,
        const std::string& user   = "postgres"  ,
        const std::string& pw     = "postgres"  ,
        const std::string& host   = "localhost" ,
        const std::string& port   = "5432"
        ) :
            conn(connection_string(dbname, user, pw, host, port))
        { }


        pqxx::result execute(const std::string& query_string)
        {
                return pqxx::nontransaction(conn).exec(query_string);
        }

private:
        pqxx::connection conn; // 96 bytes
};
