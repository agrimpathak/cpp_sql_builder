#pragma once

#include <string>


inline
std::string quote(const std::string& s)
{
    return "'" + s + "'";
}


template<typename ...B>
constexpr bool variadic_and(B&&... b)
{
    return (b && ...);
}


template<typename ...B>
constexpr bool variadic_or(B&&... b)
{
    return (b || ...);
}
