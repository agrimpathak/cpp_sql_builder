#pragma once

#include <gtest/gtest.h>
#include <insert.h>


TEST(InsertStatement, query_string)
{
    const std::string res = insert_into("foo")
                           .columns("c1","c2","c3","c4")
                           .values(1 , 0.1, "A", 'D', false)
                                  (2 , 0.2, "B", 'E', true)
                                  ("", 0.3, "C", 'F', false)
                           .on_conflict_do_nothing()
                           .returning_id()
                           .query_string();

    const std::string exp = R"SQL(INSERT INTO
foo
(c1,c2,c3,c4)
VALUES

(1,0.100000,'A','D',false),
(2,0.200000,'B','E',true),
(NULL,0.300000,'C','F',false)
ON CONFLICT DO NOTHING
RETURNING id
;)SQL";

    ASSERT_EQ(res, exp);
}
