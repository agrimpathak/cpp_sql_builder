#include <gtest/gtest.h>

#include "expression_test.h"
#include "insert_test.h"
#include "join_test.h"
#include "nullif_test.h"
#include "select_test.h"
#include "str_join_test.h"
#include "util_test.h"


int main(int argc, char** argv)
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
