#pragma once

#include <gtest/gtest.h>
#include <column.h>
#include <expression.h>


TEST(Expression, one_condition)
{
    Expression expr = column("col1") == 5.0;
    const std::string res = expr.str();
    const std::string exp = "col1 = 5.000000";
    ASSERT_EQ(res, exp);
}


TEST(Expression, two_conditions)
{
    Expression expr = column("col1") > 5.0 and column("col2") == "ABC";
    const std::string res = expr.str();
    const std::string exp = "col1 > 5.000000 AND\n col2 = 'ABC'";
    ASSERT_EQ(res, exp);
}


TEST(Expression, one_ommitted_clause_start)
{
    Expression expr = (
            column("col1") <=> ""
        and column("col2") == "ABC"
        and column("col3") == 2
        );
    const std::string res = expr.str();
    const std::string exp = "col2 = 'ABC' AND\n col3 = 2";
    ASSERT_EQ(res, exp);
}


TEST(Expression, one_ommitted_clause_middle)
{
    Expression expr = (
            column("col2") == "ABC"
        and column("col1") <=> ""
        and column("col3") == 2
        );
    const std::string res = expr.str();
    const std::string exp = "col2 = 'ABC' AND\n col3 = 2";
    ASSERT_EQ(res, exp);
}


TEST(Expression, one_ommitted_clause_end)
{
    Expression expr = (
            column("col2") == "ABC"
        and column("col3") == 2
        and column("col1") <=> ""
        );
    const std::string res = expr.str();
    const std::string exp = "col2 = 'ABC' AND\n col3 = 2";
    ASSERT_EQ(res, exp);
}


TEST(Expression, two_ommitted_clause_middle_end)
{
    Expression expr = (
            column("col3") == 2
        and column("col2") <=> ""
        and column("col1") <=> ""
        );
    const std::string res = expr.str();
    const std::string exp = "col3 = 2";
    ASSERT_EQ(res, exp);
}


TEST(Expression, two_ommitted_clause_start_end)
{
    Expression expr = (
            column("col2") <=> ""
        and column("col3") == 2
        and column("col1") <=> ""
        );
    const std::string res = expr.str();
    const std::string exp = "col3 = 2";
    ASSERT_EQ(res, exp);
}


TEST(Expression, two_ommitted_clause_start_middle)
{
    Expression expr = (
            column("col2") <=> ""
        and column("col1") <=> ""
        and column("col3") == 2
        );
    const std::string res = expr.str();
    const std::string exp = "col3 = 2";
    ASSERT_EQ(res, exp);
}


TEST(Expression, in)
{
    const std::string res = column("col").in("foo", 5.0, 1).str();
    const std::string exp = "col IN ( 'foo',5.000000,1 )";
    ASSERT_EQ(res, exp);
}


TEST(Expression, between)
{
    const std::string res = column("col").between(1,5.0).str();
    const std::string exp = "col BETWEEN 1 AND 5.000000";
    ASSERT_EQ(res, exp);
}
