#pragma once

#include <gtest/gtest.h>
#include <column.h>
#include <join.h>


struct FooStatement
{
    LeftJoin<FooStatement> left_join(const std::string& table)
    {
        return LeftJoin<FooStatement>(*this, table);
    }


    FooStatement& append_join(const std::string& s)
    {
        _s = s;
        return *this;
    }

    std::string _s;
};


TEST(LeftJoin, str)
{
    FooStatement foo;
    foo.left_join("table").on(column("c1") == column("c2") and column("c3") == 5);
    const std::string& res = foo._s;
    const std::string exp = "LEFT JOIN table ON c1 = c2 AND\n c3 = 5";
    ASSERT_EQ(res, exp);
}
