#pragma once

#include <gtest/gtest.h>
#include <util.h>


TEST(util, variadic_and)
{
    ASSERT_EQ(variadic_and(true, 1 == 1, 3.1 == 3.1, 'a' == 'a'), true);
    ASSERT_EQ(variadic_and(true, 1 == 1, 3.1 == 3.1, 'a' == 'b'), false);
}


TEST(util, variadic_or)
{
    ASSERT_EQ(variadic_or(false, 1 == 1, 3.1 == 3.2, 'a' == 'b'), true);
    ASSERT_EQ(variadic_or(false, 1 == 2, 3.1 == 3.2, 'a' == 'b'), false);
}
