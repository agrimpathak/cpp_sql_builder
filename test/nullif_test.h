#pragma once

#include <gtest/gtest.h>
#include <nullif.h>


TEST(util, nullif)
{
    const double x = 0.0;
    ASSERT_EQ(nullif(x, 0.0), "NULL");

    const double y = 1.0;
    ASSERT_EQ(nullif(y, 0.0), "1.000000");
}


TEST(util, nullif_and)
{
    ASSERT_EQ(nullif_and(2.0, 2.0 == 1.0, 1==1), "2.000000");
    ASSERT_EQ(nullif_and(2.0, 1.0 == 1.0, 1==1), "NULL");
}


TEST(util, nullif_or)
{
    ASSERT_EQ(nullif_or(2.0, 2.0 == 1.0, 1==1), "NULL");
    ASSERT_EQ(nullif_or(2.0, 2.0 == 1.0, 2==1), "2.000000");
}
