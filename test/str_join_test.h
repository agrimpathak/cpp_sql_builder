#pragma once

#include <gtest/gtest.h>
#include <string_util/str_join.h>


TEST(join, 1_args)
{
    ASSERT_EQ(join<','>(1), "1");
}


TEST(join, 2_args)
{
    ASSERT_EQ(join<','>(1, "abc"), "1,abc");
}


TEST(join, 3_args)
{
    ASSERT_EQ(join<','>(1, "abc", 7.0), "1,abc,7.000000");
}


struct FooBar
{
    template<typename T>
    std::string operator()(T&& arg) const
    {
        return "A-" + std::to_string(arg) + "-Z";
    }
};

TEST(join, custom_formatter)
{
    std::string res = join<',',FooBar>(1, "abc", 7.0);
    std::string exp = "A-1-Z,A-abc-Z,A-7.000000-Z";
    ASSERT_EQ(res, exp);
}
