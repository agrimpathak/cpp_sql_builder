#pragma once

#include <gtest/gtest.h>
#include <string>
#include <column.h>
#include <join.h>
#include <select.h>


TEST(SelectStatement, end_to_end)
{
    const std::string res =
        select("col1", "col2", "col3")
       .from("table")
       .inner_join("table2")
       .on(column("col1") == column("col9"))
       .where(
               column("col4") == 5
           and column("col5") <=> ""
           and column("col6") <= 2.0
           and column("col7").between(2,3)
           )
       .where(
               column("col8").is_null()
           or  column("col8") == 5
           )
       .group_by("col1")
       .order_by("col2", "col3")
       .query_string();
    const std::string exp = R"SQL(SELECT
col1,col2,col3
FROM
table
INNER JOIN table2 ON col1 = col9

WHERE
(col4 = 5 AND
 col6 <= 2.000000 AND
 col7 BETWEEN 2 AND 3)
AND
(col8 IS NULL OR
 col8 = 5)
GROUP BY
col1
ORDER BY
col2,col3
;)SQL";

    ASSERT_EQ(res, exp);
}
