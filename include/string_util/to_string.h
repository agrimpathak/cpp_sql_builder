#pragma once

#include <string>


namespace std
{
    inline
    string to_string(const string& s)
    {
        return s;
    }
}


struct to_string
{
    template<typename T>
    std::string operator()(T&& arg) const
    {
        return std::to_string(arg);
    }
};
