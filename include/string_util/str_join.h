#pragma once

#include <string>
#include "to_string.h"


template<char Delim, typename Formatter, typename Last>
std::string join_builder(const std::string& s, Last&& last)
{
    return s + Delim + Formatter()(last);
}


template<char Delim, typename Formatter, typename First, typename ...Rest>
std::string join_builder(const std::string& s, First&& first, Rest&&... rest)
{
    return join_builder<Delim, Formatter, Rest...>(
            s + Delim + Formatter()(first), std::forward<Rest>(rest)...);
}


template<
    char Delim                     ,
    typename Formatter = to_string ,
    typename First                 ,
    typename ...Rest
    >
std::string join(First&& first, Rest&&... rest)
{
    return join_builder<Delim, Formatter, Rest...>(
            Formatter()(first), std::forward<Rest>(rest)...);
}


template<char Delim, typename Formatter = to_string, typename First>
std::string join(First&& first)
{
    return Formatter()(first);
}


template<char Delim>
std::string join()
{
    return std::string();
}
